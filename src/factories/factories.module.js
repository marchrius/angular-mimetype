(function() {
  'use strict';

  angular.module('mg.mimetype.factories', ['mg.mimetype.utils', 'mg.mimetype.constants']);

})();