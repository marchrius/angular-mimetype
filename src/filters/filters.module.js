(function() {
  'use strict';

  angular.module('mg.mimetype.filters', ['mg.mimetype.factories']);

})();