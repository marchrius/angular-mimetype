(function() {
  'use strict';

  angular.module('mg.mimetype', ['mg.mimetype.factories', 'mg.mimetype.filters', 'mg.mimetype.directives', 'mg.mimetype.constants']);

})();